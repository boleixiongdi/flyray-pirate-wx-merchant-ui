var api = require('../../config/api.js');
var config = require('../../config/config.js');
const app = getApp();
import md5 from '../../utils/md5'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    isLogin: '1',
    username: '',
    password: '',
    come: '1'
  },

  /**
   * 登录
   */
  loginAct: function() {
    var that = this
    console.log('登录  用户名： ' + that.data.username + ' ;密码： ' + that.data.password)
    //that.data.username = 'operator001'
    //that.data.password = '123456'
    if (that.data.username == '') {
      wx.showModal({
        title: '提示',
        content: '请输入账号',
        showCancel: false, //不显示取消按钮
        confirmText: '确定'
      })
    } else {
      if (that.data.password == '') {
        wx.showModal({
          title: '提示',
          content: '请输入密码',
          showCancel: false, //不显示取消按钮
          confirmText: '确定'
        })
      }else {
        var pwd = md5(that.data.password)
        var password = md5(pwd + config.pwdSalt)
        wx.request({
          url: api.LoginUrl,
          method: "POST",
          data: {
            username: that.data.username,
            password: password
          },
          header: {
            'content-type': 'application/json'
          },
          success: function (res) {
            console.log(res)
            if (res.data.code === '0000') {
              console.log('登录成功。。。。{}')
              app.data.operatorId = res.data.userId
              if (res.data.userType == '2') {
                wx.switchTab({
                  url: '/pages/scanQr/scanQr',
                })
              } else {
                wx.redirectTo({
                  url: '/pages/device/device',
                })
              }
            }else {
              wx.showModal({
                title: '提示',
                content: '账号或密码错误',
                showCancel: false, //不显示取消按钮
                confirmText: '确定'
              })
              return false
            }
          },
          fail: function ({ errMsg }) {
            console.log('request fail', errMsg)
            wx.showModal({
              title: '提示',
              content: '请求失败',
              showCancel: false, //不显示取消按钮
              confirmText: '确定'
            })
          }
        })
      }
    }
  },

  phoneInput: function(e) {
    var that = this
    var value = e.detail.value;
    that.setData({
      username: value
    })
  },

  passwordInput: function(e) {
    var that = this
    var value = e.detail.value;
    that.setData({
      password: value
    })
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.setNavigationBarTitle({
      title: '登录'
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})