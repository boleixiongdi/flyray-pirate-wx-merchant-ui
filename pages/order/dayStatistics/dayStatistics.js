var api = require('../../../config/api.js');
var config = require('../../../config/config.js');
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    lastDayPayCount:0,
    toDayPayCount: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var platformId = options.platformId
    var merchantId = options.merchantId
    var goodsId = options.goodsId
    var that = this
    wx.request({
      url: api.DayPayStatistics,
      method: "POST",
      data: {
        platformId: platformId,
        merchantId: merchantId,
        goodsId: goodsId
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        console.log(res)
        if (res.data.code === '0000') {
          console.log('44444444444', res.data)
          if (res.data.code == '0000') {
            that.setData({
              lastDayPayCount: res.data.data.lastDayPayCount,
              toDayPayCount: res.data.data.toDayPayCount
            })

          }

        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.setNavigationBarTitle({
      title: '日付费次数统计'
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})