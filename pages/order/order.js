// pages/order/order.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.setNavigationBarTitle({
      title: '统计'
    });
  },

  dayStatistics: function () {
    wx.navigateTo({
      url: '/pages/order/merchantGoods/merchantGoods?dateType=1',
    })
  },

  monthStatistics: function () {
    wx.navigateTo({
      url: '/pages/order/merchantGoods/merchantGoods?dateType=2',
    })
  }
})