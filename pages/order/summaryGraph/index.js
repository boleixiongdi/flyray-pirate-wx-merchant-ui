import F2 from '../../../f2-canvas/lib/f2';
import data from '../../../data/sroll-line.js'
let chart = null;
var api = require('../../../config/api.js');
var config = require('../../../config/config.js');
const app = getApp();

function initChart(canvas, width, height) {
  
  chart = new F2.Chart({
    el: canvas,
    width,
    height,
    animate: false
  });

  for (var i = 0; i < app.data.statisticsList.length; i++){
    //获取当前月份
    var myDate = new Date();
    var month = myDate.getMonth();

    // if (i == month){
      // 绘制 tag

      var count = app.data.statisticsList[i].count;
      chart.guide().tag({
        position: [i, count],
        withPoint: false,
        content: count,
        limitInPlot: true,
        offsetX: 1,
        direct: 'cr'
      });
    // }
  }
  chart.source(app.data.statisticsList, {
    release: {

    }
  });
  chart.tooltip({
    showCrosshairs: false,
    showItemMarker: false,
    background: {
      radius: 1,
      fill: '#1890FF',
      padding: [3, 4]
    },
    nameStyle: {
      fill: '#fff'
    },
    onShow(ev) {
      const items = ev.items;
      items[0].name = items[0].title;
    }
  });
  chart.line().position('release*count');
  chart.point()
    .position('release*count')
    .style({
      lineWidth: 1,
      stroke: '#fff'
    });

  chart.interaction('pan');
  // 定义进度条
  chart.scrollBar({
    mode: 'x',
    xStyle: {
      offsetY: -1
    }
  });
  
  chart.render();
  return chart;
}

Page({
  onShareAppMessage: function (res) {
    return {
      title: 'F2 微信小程序图表组件，你值得拥有~',
      path: '/pages/index/index',
      success: function () { },
      fail: function () { }
    }
  },

  data: {
    opts: {
      onInit: initChart
    },
    platformId:'',
    merchantId:'',
    goodsId:'',
    lastDayPayCount: 0,
    toDayPayCount: 0
  },

  onReady() {
  },

  /**
 * 生命周期函数--监听页面显示
 */
  onShow: function () {
    wx.setNavigationBarTitle({
      title: '付费次数统计'
    });
  },

  onLoad: function (options) {
    var platformId = options.platformId
    var merchantId = options.merchantId
    var goodsId = options.goodsId
    var that = this
    wx.request({
      url: api.DayPayStatistics,
      method: "POST",
      data: {
        platformId: platformId,
        merchantId: merchantId,
        goodsId: goodsId
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        console.log(res)
        if (res.data.code === '0000') {
          console.log('44444444444', res.data)
          if (res.data.code == '0000') {
            that.setData({
              lastDayPayCount: res.data.data.lastDayPayCount,
              toDayPayCount: res.data.data.toDayPayCount
            })

          }

        }
      }
    })
  },

  
});
