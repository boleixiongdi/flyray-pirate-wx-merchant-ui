var api = require('../../../config/api.js');
var config = require('../../../config/config.js');
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    merchantList: [],

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    wx.setNavigationBarTitle({
      title: '付费次数统计'
    });

    var that = this
    wx.request({
      url: api.QueryMerchantGoodsList,
      method: "POST",
      data: {
        operatorId: app.data.operatorId
      },
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        console.log(res)
        if (res.data.code === '0000') {
          console.log('44444444444', res.data)
          if (res.data.code == '0000'){
            that.setData({
              merchantList: res.data.data.merchantList
            })
            
          }
          
        }
      }
    })

    
  },

  statistics: function (e){
    console.log("222222222222",e)
    var that = this;
    var goodsId = e.currentTarget.id;
    var merchantId = e.currentTarget.dataset.merchantid;
    var platformId = e.currentTarget.dataset.platformid;

        wx.request({
          url: api.QueryThenPayStatisticsList,
          method: "POST",
          data: {
            platformId: platformId,
            merchantId: merchantId,
            goodsId: goodsId,
          },
          header: {
            'content-type': 'application/json'
          },
          success: function (res) {
            console.log("555555555555555", res)
            if (res.data.code === '0000') {
              console.log('44444444444', res.data)
              if (res.data.code == '0000') {

                app.data.statisticsList = res.data.data.statisticsList
                wx.navigateTo({
                  url: '/pages/order/summaryGraph/index?platformId=' + platformId + "&merchantId=" + merchantId + "&goodsId=" + goodsId,
                })
              }

            }
          }
        })
        

  }
})