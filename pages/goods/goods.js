var api = require('../../config/api.js');
var config = require('../../config/config.js');
const app = getApp();


Page({

  /**
   * 页面的初始数据
   */
  data: {
    goodsId: '',
    merchantId: '',
    goodsPic: '',
    hasBind: false,
    merchantList: [],
    casArray: ['商户1', '商户2', '商户3', '商户4', '商户5'],
    casIndex: -1,
    merchantName: '',
    index: 0
  },

  bindMerchant: function() {
    var that = this
    wx.request({
      url: api.bindMerchant,
      header: {
        "content-type": "application/json"
      },
      method: "POST",
      data: {
        platformId: config.platformId,
        goodsId: that.data.goodsId,
        merchantId: app.data.merchantId
      },
      success: function (res) {
        console.log('绑定' + res)
        that.setData({
          hasBind: true
        })
      }
    })
  },

  cancelBind: function() {
    var that = this
    wx.request({
      url: api.cancelBindMerchant,
      header: {
        "content-type": "application/json"
      },
      method: "POST",
      data: {
        platformId: config.platformId,
        goodsId: that.data.goodsId
      },
      success: function (res) {
        that.setData({
          hasBind: false,
          casIndex: -1
        })
        that.getGoodsInfo()
      }
    })
  },

  bindCasPickerChange: function (e) {
    console.log('乔丹选的是', this.data.casArray[e.detail.value].merchantId)
    this.setData({
      casIndex: e.detail.value
    })
    app.data.merchantId = this.data.casArray[e.detail.value].merchantId

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    that.setData({
      goodsId: options.goodsId
    })
    that.getGoodsInfo()
  },

  /**
   * 商品详情
   */
  getGoodsInfo: function() {
    var that = this
    wx.request({
      url: api.GoodsInfo,
      header: {
        "content-type": "application/json"
      },
      method: "POST",
      data: {
        platformId: config.platformId,
        goodsId: that.data.goodsId
      },
      success: function (res) {
        console.log(res.data.data)
        if (res.data.data.merchantId != '' && res.data.data.merchantId != null) {
          console.log('啥玩意')
          that.setData({
            goodsId: res.data.data.goodsId,
            merchantId: res.data.data.merchantId,
            goodsPic: res.data.data.goodsPic,
            hasBind: true
          })
          //获取商户名称
          wx.request({
            url: api.MerchantInfoQuery,
            header: {
              "content-type": "application/json"
            },
            method: "POST",
            data: {
              platformId: config.platformId,
              merchantId: res.data.data.merchantId
            },
            success: function (res) {
              console.log('商户name ' + res.data)
              that.setData({
                merchantName: res.data.merchantName
              })
            }
          })
        }else {
          console.log('哎~')
          that.setData({
            goodsId: res.data.data.goodsId,
            goodsPic: res.data.data.goodsPic
          })
        }
        if(that.data.hasBind == false) {
          console.log('哎~SSS')
          that.getMerchants()
        }
      }
    })
  },

  /**
   * 获取商户
   */
  getMerchants: function() {
    console.log('运营人员id ' + app.data.operatorId)
    var that = this
    wx.request({
      url: api.MerchantQuery,
      header: {
        "content-type": "application/json"
      },
      method: "POST",
      data: {
        platformId: config.platformId,
        operatorId: app.data.operatorId
      },
      success: function (res) {
        console.log("555555",res.data.data)
        var array = res.data.data
        var arr = [];
        for (var i = 0; i < array.length; i++){
          var merchantName = array[i].merchantName
          arr.push(merchantName);
        }
        that.setData({
          casArray: arr
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})