// pages/scanQr/scanQr.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    qrContext: ''
  },

  saoma: function() {
    var that = this
    wx.scanCode({
      success: (res) => {
        console.log(res)
        var result = res.result
        var res1 = result.split('?')[1] 
        var goodsId = res1.split('&')[0]
        var gid = goodsId.split('=')[1]
        console.log('商品id '+gid)       
        if(gid != '') {
          wx.navigateTo({
            url: '/pages/goods/goods?goodsId='+gid,
          })
        }else {
          wx.showToast({
            title: '商品信息不存在',
            icon: 'success',
            duration: 2000
          })
        }
      },
      fail: (res) => {
        wx.showToast({
          title: '失败',
          icon: 'success',
          duration: 2000
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.setNavigationBarTitle({
      title: '扫描'
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})