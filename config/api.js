
//请求url
// var API_URL = "http://192.168.1.151:8765/api";

var API_URL = "http://www.luckingcharge.com/api/api";

module.exports = {
  //登录
  LoginUrl: API_URL + '/auth/authorization/operator/token',

  //商品详情
  GoodsInfo: API_URL + '/thymeleaf/qrGoods/info',

  //商户列表
  MerchantQuery: API_URL + '/crm/merchants/list',

  //获取商户商品列表
  QueryMerchantGoodsList: API_URL + '/thymeleaf/qrGoods/queryMerchantGoodsList',

  //查询当年该商品支付次数统计
  QueryThenPayStatisticsList: API_URL + '/thymeleaf/qrGoods/queryThenPayStatisticsList',

  //日付费次数统计 昨天与今天
  DayPayStatistics: API_URL + '/thymeleaf/qrGoods/dayPayStatistics',

  //商户信息
  MerchantInfoQuery: API_URL + '/crm/merchants/info',

  //绑定商户
  bindMerchant: API_URL + '/thymeleaf/qrGoods/doBind',

  //取消绑定商户
  cancelBindMerchant: API_URL + '/thymeleaf/qrGoods/cancelBind',

  
}